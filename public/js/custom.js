var pageNumber = 2;

var load_more_rec_id;

var get_chat_rec_id;



function check_validation() {
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: base_url + "/verify-otp",
        method: "post",
        dataType: "json",
        data: {
            "phone": $("#phone").val(),
            "cc": $("#cc").val(),
            "otp": $("#otp").val()
        },

        success: function (response) {

            console.log(response);

            if (response.status == "success") {
                $("#reg-form").submit();
            }
        },
        error: function (response) {
            console.log(response.responseJSON);
            alert(response.responseJSON.msg);
        }
    })
}

function send_otp() {
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: base_url + "/send-otp",
        method: "post",
        dataType: "json",
        data: {
            "phone": $("#phone").val(),
            "cc": $("#cc").val()
        },

        success: function (response) {

            if (response.status == "success") {
                alert("Otp sent successfully");
            }
        },
        error: function (response) {
            console.log(response.responseJSON);
            alert(response.responseJSON.msg);
        }
    })
}

function get_users() {
    $.ajax({
        url: base_url + "/get-users",
        method: "get",
        dataType: "json",
        success: function (response) {

            if (response.status == "success") {
                $("#chat-users").empty();
                for (var i = 0; i < response.result.length; i++) {
                    var msg = "";
                    var time = "";
                    var red_dot = ``;
                    var green_dot = ``;
                    if (response.result[i].last_chat) {
                        if (response.result[i].last_chat.file_type == 0) {
                            msg = `<p>` + response.result[i].last_chat.message + `</p>`;
                            time = `<span>` + response.result[i].last_chat.time + `</span>`;
                        } else {
                            msg = "File Attchment";
                            time = `<span>` + response.result[i].last_chat.time + `</span>`;
                        }

                        if (response.result[i].last_chat_status == 0) {
                            red_dot = `<span class="red-dot"></span>`;
                        }

                    }
                    if (response.result[i].is_login == 0) {
                        green_dot = `<p class="green-dot"></p>`;
                    }
                    $("#chat-users").append(`<li> ` + green_dot + `
                                                    <a href="#chat1" onclick="get_chats(` + response.result[i].id + `);read_msg(` + response.result[i].id + `);" class="filter direct active chat-u" data-chat="open" data-toggle="tab" role="tab" aria-controls="chat1" aria-selected="true">
                                                        <div class="status online"><img src=` + base_url + `/public/dist/img/avatars/avatar-male-1.jpg alt="avatar"><i data-eva="radio-button-on"></i></div>
                                                        <div class="content">
                                                            <div class="headline">
                                                                ` + red_dot + `
                                                                <h5>` + response.result[i].name + `</h5>
                                                                 ` + time + ` 
                                                            </div>
                                                           ` + msg + ` 
                                                        </div>
                                                    </a>
                                                </li>`);

                    var rec_id = $("#rec_id").val();

                    // if(rec_id != undefined){
                    //     rec_id = rec_id.replace(/\//g,'');
                    //     if(load_more_rec_id!=undefined && get_chat_rec_id != undefined){
                    //         load_more_rec_id = load_more_rec_id.replace(/\//g,'');
                    //     }
                    //     if(get_chat_rec_id == load_more_rec_id){
                    //         get_chats(rec_id,pageNumber);
                    //     }else{
                    //         get_chats(rec_id);
                    //     }
                    //
                    // }

                }

            }

        }
    })
}

function send_msg(rec_id) {
    console.log($("#msg").val());
    if ($("#msg").val() == "") {
        $("#msg").css("border:1px solid red")
    }
    else {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: base_url + "/send-msg/" + rec_id,
            method: "post",
            dataType: "json",
            data: {
                msg: $("#msg").val()
            },
            success: function (response) {

                if (response.status == "success") {


                    $("#msg").val("");
                    get_users();
                    get_chats(rec_id);

                }
            }
        })
    }
}


function scrollToDown() {
    $('#scroll').animate({scrollTop: $('#chat-msgs').prop("scrollHeight")}, 100);
}


function load_more() {

    var rec_id = $("#rec_id").val();
    load_more_rec_id = rec_id;
    $.ajax({
        url: base_url + "/load-more/" + rec_id,
        method: "get",
        dataType: "json",
        data: {
            offset: pageNumber
        },
        success: function (response) {


            if (response.status == "success") {
                $("#load-more").empty();
                if (response.rem > 0) {
                    $("#load-more").append(`<b onclick="load_more()">Load Older Messages</b>`);
                }
               // if(get_chat_rec_id == load_more_rec_id){
                    pageNumber += 1;
                //}

                $("#chat-msgs").empty();
                $("#user-name").empty();
                for (var i = response.result.length - 1; i >= 0; i--) {


                    $("#chat-msgs").append(`<p class="green text-center text-bold">` + response.result[i].date + `</p>`);


                    value = response.result[i].value;



                    for (var j = value.length-1; j >= 0; j--) {


                        if (auth_user_id == value[j].sender_id) {
                            var bubble = `<div class="bubble-blue">`;
                            var li_chat = `<li class="li-right">`;
                        } else {
                            var bubble = `<div class="bubble-gray">`;
                            var li_chat = `<li class="li-left">`;
                        }
                        var seen = "";

                        if (auth_user_id == value[j].sender_id && value[j].status) {
                            seen = "Seen";
                        }

                        if (value[j].file_type == 0) {
                            $("#chat-msgs").append(`` + li_chat + `
                                                 <div class="content">
                                                    <div class="message">
                                                        ` + bubble + `
                                                        <input type="hidden" id="rec_id" value=` + rec_id + `/>
                                                            <p>` + value[j].message + `</p>
                                                        </div>
                                                    </div>
                                                    <span>` + seen + ` ` + value[j].date + `</span>
                                                </div>
                                            </li>`)
                        } else {
                            $("#chat-msgs").append(li_chat + `
                                                 <div class="content">
                                                    <div class="message">
                                                        ` + bubble + `
                                                            <p><a href="` + value[j].message + `">File Attachment</a></p>
                                                        </div>
                                                    </div>
                                                    <span>` + seen + ` ` + value[j].date + `</span>
                                                </div>
                                            </li>`)
                        }
                    }
                }
            }
        }
    })
}


function get_chats(rec_id,page=null) {
    console.log(page);
    if(!page){
      var  url = base_url + "/get-chats/" + rec_id
    }else{
     var  url = base_url + "/get-chats/" + rec_id + "?offset="+page;
    }

    get_chat_rec_id = rec_id;

    $.ajax({
        url: url,
        method: "get",
        dataType: "json",

        success: function (response) {

            if (response.status == "success") {

                pageNumber = 2;

                if(get_chat_rec_id == load_more_rec_id){
                    pageNumber += 1;
                }

                $("#load-more").empty();
                if (response.rem > 0) {
                    $("#load-more").append(`<b onclick="load_more()">Load Older Messages</b>`);
                }


                $("#chat-msgs").empty();
                $("#user-name").empty();
                $("#user-typing").empty();
                for (var i = response.result.length - 1; i >= 0; i--) {


                    $("#chat-msgs").append(`<p class="green text-center text-bold">` + response.result[i].date + `</p>`);

                    value = response.result[i].value;



                    for (var j = value.length-1; j >= 0; j--) {


                        if (auth_user_id == value[j].sender_id) {
                            var bubble = `<div class="bubble-blue">`;
                            var li_chat = `<li class="li-right">`;
                        } else {
                            var bubble = `<div class="bubble-gray">`;
                            var li_chat = `<li class="li-left">`;
                        }
                        var seen = "";

                        if (auth_user_id == value[j].sender_id && value[j].status) {
                            seen = "Seen";
                        }

                        if (value[j].file_type == 0) {
                            $("#chat-msgs").append(`` + li_chat + `
                                                 <div class="content">
                                                    <div class="message">
                                                        ` + bubble + `
                                                        <input type="hidden" id="rec_id" value=` + rec_id + `/>
                                                            <p>` + value[j].message + `</p>
                                                        </div>
                                                    </div>
                                                    <span>` + seen + ` ` + value[j].date + `</span>
                                                </div>
                                            </li>`)
                        } else {
                            $("#chat-msgs").append(li_chat + `
                                                 <div class="content">
                                                    <div class="message">
                                                        ` + bubble + `
                                                            <p><a href="` + value[j].message + `">File Attachment</a></p>
                                                        </div>
                                                    </div>
                                                    <span>` + seen + ` ` + value[j].date + `</span>
                                                </div>
                                            </li>`)
                        }
                    }
                }

                // $("#write-msg").append(`
                //                             <div class="bottom">
                //
                //                                 <div class="input-group mb-3">
                //                                     <textarea class="form-control" id="msg"  placeholder="Type message..." rows="1" required onfocus="read_msg(` + rec_id + `);""></textarea>
                //                                     <input type="hidden" id="rec_id" value="` + rec_id + `"/>
                //                                     <div class="input-group-append">
                //                                     <button type="button"  class="btn-primary" onclick="send_msg(` + rec_id + `)">Send</button>
                //                                     <button type="button"  class="btn-primary" onclick="select_file()">File</button>
                //                                 <input class="d-none" type="file" id="image" onchange="upload_file(` + rec_id + `)"/>
                //                                 </div></div>
                //
                //                             </div>
                //                         `);

                $("#user-name").append(`<img src=` + base_url + `/public/dist/img/avatars/avatar-male-3.jpg alt="avatar">
                                            <div class="content">
                                                <h5>` + response.rec_name + `</h5>
                                              
                                            </div>`);


                // $("#user-typing").append(`<button class="btn btn-default" onclick="close_chat(`+rec_id+`)">Close Chat</button>`);
                // scrollToDown();


                check_chat_status(rec_id);


            }
        }
    })
}


function close_chat(rec_id){
    $.ajax({
        url: base_url + "/close-chat/" + rec_id,
        method: "get",
        dataType: "json",
        success: function (response) {

            check_chat_status(rec_id);
            }

        });
}

function check_chat_status(rec_id){
    $.ajax({
        url: base_url + "/check-chat-status/" + rec_id,
        method: "get",
        dataType: "json",
        success: function (response) {
            if (response.chat_status == "closed") {
                $("#write-msg").empty();
                $("#user-typing").html(`<button class="btn btn-default">Closed</button>`);
            } else {
                $("#write-msg").append(`
                                            <div class="bottom">
                                                
                                                <div class="input-group mb-3">
                                                    <textarea class="form-control" id="msg"  placeholder="Type message..." rows="1" required onfocus="read_msg(` + rec_id + `);""></textarea>
                                                    <input type="hidden" id="rec_id" value="` + rec_id + `"/>
                                                    <div class="input-group-append">
                                                    <button type="button"  class="btn-primary" onclick="send_msg(` + rec_id + `)">Send</button>
                                                    <button type="button"  class="btn-primary" onclick="select_file()">File</button>
                                                <input class="d-none" type="file" id="image" onchange="upload_file(` + rec_id + `)"/>
                                                </div></div>
                                                
                                            </div>
                                        `);
                if(auth_user_type == 1){
                    $("#user-typing").html(`<button class="btn btn-default" onclick="close_chat(` + rec_id + `)">Close Chat</button>`);
                }

                scrollToDown();
            }
        }
    });
}




function send_to_enter(event) {
    console.log("calling");
    if (event.keyCode == 13) {
        event.preventDefault();
        var rec_id = $("#rec_id").val();
        send_msg(rec_id);
    }
}

function user_typing(rec_id) {

    $.ajax({
        url: base_url + "/user-typing/" + rec_id,
        method: "get",
        dataType: "json",
        success: function (response) {

            if (response.status == "success") {

                $("#user-typing").html("Typing.....");
            }
        }, error: function (response) {
            $("#user-typing").html("");
        }

    });
}


function read_msg(rec_id) {
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: base_url + "/read-msg/" + rec_id,
        method: "get",
        dataType: "json",

        success: function (response) {

            if (response.status == "success") {


                $("#msg").val("");
                get_users();
                get_chats(rec_id);

            }
        }
    })

}

function select_file() {
    $("#image").click();
}

function upload_file(id) {
    var file = document.getElementById('image').files[0];
    var formData = new FormData();
    formData.append('image', file);
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: 'upload-file/' + id,
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: function (result) {
            get_chats(id);

        },
        error: function (data) {
            console.log(data);
        }

    });
}
