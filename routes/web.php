<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\User;
use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/get-users', 'HomeController@get_users');
Route::get('/get-chats/{id}', 'HomeController@get_chats');
Route::get('/close-chat/{id}', 'HomeController@close_chat');
Route::get('/check-chat-status/{id}', 'HomeController@check_chat_status');
Route::get('/load-more/{id}', 'HomeController@load_more');
Route::get('/read-msg/{id}', 'HomeController@read_msg');
Route::get('/user-typing/{id}', 'HomeController@user_typing');
Route::post('/send-msg/{id}', 'HomeController@send_msg');
Route::post('/upload-file/{id}', 'HomeController@upload_file');
Route::post('/send-otp', 'Auth\RegisterController@send_otp');
Route::post('/verify-otp', 'Auth\RegisterController@verify_otp');
Route::post('/user/logout', function (){
    $u = User::find(Auth::user()->id);
    $u->is_login = 1;
    $u->save();
    Auth::logout();
    return redirect("/");
});
