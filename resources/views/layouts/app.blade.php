<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <!-- Scripts -->
    <script src="{{ asset('public/js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">


    <title>Swipe – The Simplest Chat Platform</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="#">
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('public/dist/css/lib/bootstrap.min.css')}}" type="text/css" rel="stylesheet">
    <!-- Swipe core CSS -->
    <link href="{{ asset('public/dist/css/swipe.min.css')}}" type="text/css" rel="stylesheet">
    <!-- Favicon -->
    <link href="{{ asset('public/dist/img/favicon.png')}}" type="image/png" rel="icon">

    <link href="{{ asset('public/css/custom.css')}}" type="text/css" rel="stylesheet">

</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                {{ Auth::user()->name }}
                            </a>

                            <form id="logout-form" action="{{ URL::to('/')}}/user/logout" method="POST" style="display: none;">
                                @csrf
                            </form>

                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ URL::to('/')}}/user/logout"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>


    @yield('content')


</div>


<script src="{{ asset('public/dist/js/vendor/jquery-slim.min.js')}}"></script>
<script src="{{ asset('public/dist/js/vendor/popper.min.js')}}"></script>
<script src="{{ asset('public/dist/js/vendor/feather.min.js')}}"></script>
<script src="{{ asset('public/dist/js/vendor/eva.min.js')}}"></script>
<script src="{{ asset('public/dist/js/vendor/bootstrap.min.js')}}"></script>
<script src="{{ asset('public/dist/js/swipe.min.js')}}"></script>
<script src="https://js.pusher.com/5.0/pusher.min.js"></script>


<script>

    // Enable pusher logging - don't include this in production

</script>

<script>
@if(Auth::user())

    var auth_user_id = "{{ Auth::user()->id }}";

    $(document).ready(function () {
        get_users();
    });

    const interval = setInterval(function () {
       get_users();
    }, 5000);



    var auth_user_type = "{{Auth::user()->user_type}}";


@endif
    var base_url = "{{URL::to('/')}}";
</script>


<script src="{{ asset('public/js/custom.js')}}"></script>


</body>
</html>
