<?php

namespace App\Http\Controllers\Auth;

use App\Otp;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'phone'=>['required'],
            'otp'=>['required']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {


        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'user_type'=>2,
            'phone'=>"+".$data['cc'].$data['phone'],
            'phone_verified'=>1
        ]);
    }

    public function send_otp(Request $request){
        $phone = $request->phone;
        $cc = $request->cc;
        $otp = rand(1000,9999);
        $expiry_date = strtotime("+5 minutes");

        $u = User::where("phone","+".$cc.$phone)->first();
        if($u){
            return response()->json([
                'status' => 'error',
                'msg'=>'phone already taken'
            ],400);
        }

        Otp::where("phone","+".$cc.$phone)->delete();

        $o = new Otp();
        $o->phone = "+".$cc.$phone;
        $o->otp = $otp;
        $o->expiry_date = $expiry_date;
        $o->save();

        $res = send_otp($phone,"Your Verification Code is ",$otp);

        if($res == true){
            return response()->json([
                'status' => 'success',
                'result' => $o
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'msg'=>'Wrong or expired OTP'
            ],400);
        }
    }

    public function verify_otp(Request $request){
        $phone = $request->phone;
        $cc = $request->cc;
        $otp = $request->otp;

        $res = Otp::where(["phone"=>"+".$cc.$phone,"otp"=>$otp])->first();

        if($res){
            Otp::where("phone","+".$cc.$phone)->delete();
            return response()->json([
                'status' => 'success',
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'msg'=>'Otp is wrong or expired'
            ],400);
        }

    }


}
