<?php

namespace App\Http\Controllers;

use App\ChatStatus;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Chat;
use DB;
use App\MyEvent;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $u = User::find(Auth::user()->id);
        $u->is_login = 0;
        $u->save();
        return view('index');
    }

    public function get_users()
    {
        $user = Auth::user();
        $user_type = $user->user_type;
        $auth_user = $user->id;

        if ($user_type == 1) {

            //$res = DB::table("chat_users")->where("user_type",2)->get();

            $res = User::where("user_type", 2)->get();
            foreach ($res as $k=>$r) {

                $r->last_chat = Chat::latest()
                    ->where("sender_id", $auth_user)
                    ->where("rec_id", $r->id)
                    ->orwhere("sender_id", $r->id)
                    ->where("rec_id", $auth_user)->first();

                if ($r->last_chat) {
                    $r->last_chat->time = date("d/m/Y h:iA", $r->last_chat->time);
                }

                $r->last_chat_status = DB::table("chats")
                    ->where("rec_id", $auth_user)
                    ->where("sender_id", $r->id)
                    ->orderby("id", "desc")
                    ->pluck("status")
                    ->first();

                $new[$k] = $r->last_chat_status;

            }

            $res = json_decode($res);
            array_multisort($new, SORT_ASC, $res);

        } else {
            $res = User::where("user_type", 1)->get();
            foreach ($res as $r) {
                $r->last_chat = DB::table("chats")->select("message", "file_type", "time", "status")->where("sender_id", $auth_user)
                    ->where("rec_id", $r->id)
                    ->orwhere("sender_id", $r->id)
                    ->where("rec_id", $auth_user)
                    ->orderby("id", "desc")
                    ->first();

                if ($r->last_chat) {
                    $r->last_chat->time = date("d/m/Y h:iA", $r->last_chat->time);
                }


                $r->last_chat_status = DB::table("chats")
                    ->where("rec_id", $auth_user)
                    ->where("sender_id", $r->id)
                    ->orderby("id", "desc")
                    ->pluck("status")
                    ->first();

            }
        }
        return response()->json([
            'status' => 'success',
            'result' => $res
        ], 200);
    }

    public function get_chats($rec_id,Request $request)
    {
        $rec_name = User::find($rec_id);
        $rec_name = $rec_name->name;
        $sender_id = Auth::user()->id;
        $offset = $request->offset;
        if(!$offset){
            $offset = 1;
        }
        $date = Chat::where("sender_id", $sender_id)
            ->where("rec_id", $rec_id)
            ->orwhere("sender_id", $rec_id)
            ->where("rec_id", $sender_id)
            ->orderBy('time','desc')
            ->select(DB::raw('DATE_FORMAT(FROM_UNIXTIME(time), "%d-%m-%Y") AS date'))
            ->groupBy('date')
            ->limit(1*$offset)
            ->get();


        foreach($date as $k => $d)
        {
            $date[$k]->value = DB::table('chats')
                ->select("*",DB::raw("date_format(from_unixtime(time),'%d-%m-%Y %h:%i:%p') as date"))
                ->where("sender_id", $sender_id)
                ->where("rec_id", $rec_id)
                ->where(DB::raw('DATE_FORMAT(FROM_UNIXTIME(time), "%d-%m-%Y")'), '=', $d->date)
                ->orwhere("sender_id", $rec_id)
                ->where("rec_id", $sender_id)
                ->where(DB::raw('DATE_FORMAT(FROM_UNIXTIME(time), "%d-%m-%Y")'), '=', $d->date)
                ->orderby("id","desc")
                ->get();

            if($d->date == date('d-m-Y')) {
                $d->date = 'Today';
            }
            else if($d->date == date('d-m-Y',time() - (24 * 60 * 60))) {
                $d->date = 'Yesterday';
            }
        }

//
//        foreach ($date as $k => $d){
//
//        }

//        $res = DB::table("chats")->where("sender_id", $sender_id)
//            ->where("rec_id", $rec_id)
//            ->orwhere("sender_id", $rec_id)
//            ->where("rec_id", $sender_id)
//            ->orderby("id","desc")
//            ->limit(5)
//            ->get();

        $total_count = count(Chat::where("sender_id", $sender_id)
            ->where("rec_id", $rec_id)
            ->orwhere("sender_id", $rec_id)
            ->where("rec_id", $sender_id)
            ->orderBy('time','desc')
            ->select(DB::raw('DATE_FORMAT(FROM_UNIXTIME(time), "%d-%m-%Y") AS date'))
            ->groupBy('date')
            ->get());

//        $total_count =   count(DB::table("chats")->where("sender_id", $sender_id)
//            ->where("rec_id", $rec_id)
//            ->orwhere("sender_id", $rec_id)
//            ->where("rec_id", $sender_id)
//            ->orderby("id","desc")
//            ->get());

        $rem = $total_count - 1;

//        foreach ($res as $r) {
//            $r->time = date("d/m/Y h:iA", $r->time);
//        }
        return response()->json([
            'status' => 'success',
            'result' => $date,
            'rem' => $rem,
            'rec_name' => $rec_name
        ], 200);
    }
     public function load_more($rec_id,Request $request)
        {
            $rec_name = User::find($rec_id);
            $rec_name = $rec_name->name;
            $sender_id = Auth::user()->id;
            $offset = $request->offset;

//            $res = DB::table("chats")->where("sender_id", $sender_id)
//                ->where("rec_id", $rec_id)
//                ->orwhere("sender_id", $rec_id)
//                ->where("rec_id", $sender_id)
//                ->take($offset*5)
//                ->orderby("id","desc")
//                ->get();


         $date = Chat::where("sender_id", $sender_id)
             ->where("rec_id", $rec_id)
             ->orwhere("sender_id", $rec_id)
             ->where("rec_id", $sender_id)
             ->orderBy('time','desc')
             ->select(DB::raw('DATE_FORMAT(FROM_UNIXTIME(time), "%d-%m-%Y") AS date'))
             ->groupBy('date')
             ->limit($offset*1)
             ->get();


         foreach($date as $k => $d)
         {

             $date[$k]->value = DB::table('chats')
                 ->select("*",DB::raw("date_format(from_unixtime(time),'%d-%m-%Y %h:%i:%p') as date"))
                 ->where("sender_id", $sender_id)
                 ->where("rec_id", $rec_id)
                 ->where(DB::raw('DATE_FORMAT(FROM_UNIXTIME(time), "%d-%m-%Y")'), '=', $d->date)
                 ->orwhere("sender_id", $rec_id)
                 ->where("rec_id", $sender_id)
                 ->where(DB::raw('DATE_FORMAT(FROM_UNIXTIME(time), "%d-%m-%Y")'), '=', $d->date)
                 ->orderby("id","desc")
                 ->get();

             if($d->date == date('d-m-Y')) {
                 $d->date = 'Today';
             }
             else if($d->date == date('d-m-Y',time() - (24 * 60 * 60))) {
                 $d->date = 'Yesterday';
             }

         }



//         $total_count =   count(DB::table("chats")->where("sender_id", $sender_id)
//                ->where("rec_id", $rec_id)
//                ->orwhere("sender_id", $rec_id)
//                ->where("rec_id", $sender_id)
//                ->orderby("id","desc")
//                ->get());

         $total_count = count(Chat::where("sender_id", $sender_id)
             ->where("rec_id", $rec_id)
             ->orwhere("sender_id", $rec_id)
             ->where("rec_id", $sender_id)
             ->orderBy('time','desc')
             ->select(DB::raw('DATE_FORMAT(FROM_UNIXTIME(time), "%d-%m-%Y") AS date'))
             ->groupBy('date')
             ->get());


            $rem = $total_count - $offset*1;

//            foreach ($res as $r) {
//                $r->time = date("d/m/Y h:iA", $r->time);
//            }
            return response()->json([
                'status' => 'success',
                'result' => $date,
                'rem' => $rem,
                'rec_name' => $rec_name
            ], 200);
        }


    public function read_msg($rec_id)
    {
        $rec_name = User::find($rec_id);
        $rec_name = $rec_name->name;
        $sender_id = Auth::user()->id;
        DB::enableQueryLog();
        $res = DB::table("chats")->where("sender_id", $rec_id)
            ->where("rec_id", $sender_id)
            ->update(["status" => 1]);


        return response()->json([
            'status' => 'success',

        ], 200);
    }

    public function user_typing($rec_id)
    {
        $sender_id = Auth::user()->id;
        if ($rec_id == $sender_id) {
            return response()->json([
                'status' => 'success', 200]);
        } else {
            return response()->json([
                'status' => 'error', 400]);
        }
    }

    public function send_msg($id, Request $request)
    {

        $rec_id = $id;

        $sender_id = $user = Auth::user()->id;
        $msg = $request->input("msg");
        $time = time();
        $file_type = 0;

        $cs = ChatStatus::where("sender_id",$rec_id)
            ->where("sender_id",$sender_id)
            ->orwhere("rec_id",$rec_id)
            ->where("rec_id",$sender_id)
            ->first();
        if(!$cs){
            $chat = new ChatStatus();
            $chat->sender_id = $sender_id;
            $chat->rec_id = $rec_id;
            $chat->save();
        }


        DB::select('call send_msg(?,?,?,?,?)', array($sender_id, $rec_id, $msg, $file_type, $time));

        return response()->json([
            'status' => 'success', 'res' => ["sender_id" => $sender_id, "rec_id" => $rec_id, "message" => $msg, "file_type" => 0, "time" => time()]
        ], 200);
    }

    public function  close_chat($rec_id){
        $cs = ChatStatus::where("sender_id",$rec_id)->orwhere("rec_id",$rec_id)->first();
        if($cs){
            $cs->status = 1;
            $cs->save();
        }

        return response()->json([
            'status' => 'success'
        ], 200);
    }
      public function  check_chat_status($rec_id){
            $user_type = Auth::user()->user_type;
               $cs = ChatStatus::where("sender_id",$rec_id)
                   ->orwhere("rec_id",$rec_id)
                    ->first();

                if($cs){
                    if($cs->status == 1){

                        return response()->json([
                            'status' => 'success',
                            'chat_status'=>'closed'
                        ], 200);
                    }else{

                        return response()->json([
                            'status' => 'success',
                            'chat_status'=>'open'
                        ], 200);
                    }
                }else{

                    return response()->json([
                        'status' => 'success',
                        'chat_status'=>'open'
                    ], 200);
                }




        }

    public function upload_file($id, Request $request)
    {
        $rec_id = $id;

        $sender_id = $user = Auth::user()->id;
        $file = $request->file("image");
        $destinationPath = 'uploads';
        $file->move($destinationPath, $file->getClientOriginalName());
        $msg = $destinationPath . "/" . $file->getClientOriginalName();

        $time = time();
        $file_type = 1;

        DB::select('call send_msg(?,?,?,?,?)', array($sender_id, $rec_id, $msg, $file_type, $time));

        return response()->json([
            'status' => 'success'
        ], 200);

    }
}
